-- SUMMARY --

The Simplenews Conditions module add a checkbox for your terms 
and conditions. For the custom path to your terms and conditions 
visit the page:
admin/config/services/simplenews/settings/conditions

For a full description of the module, visit the sandbox page of this module:
http://drupal.org/sandbox/luukyb/1215234

To submit bug reports and feature suggestions, or to track changes:
http://drupal.org/project/issues/1215234


-- REQUIREMENTS --

* Simplenews module - http://drupal.org/project/simplenews


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONTACT --

Current maintainer:
* Luc Bezier (Luukyb) - http://drupal.org/user/692278
